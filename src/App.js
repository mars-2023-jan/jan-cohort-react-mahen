import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import SubHeader from './components/SubHeader';
import Expense from './components/Expense';
import Reducer from './components/Reducer';

 function App() {
  return (
    //JSX
    <div className="App">
      <Reducer />
     
    </div>
  );
}

export default App;
